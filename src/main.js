import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'bulma/css/bulma.css'
import VueSweetalert2 from 'vue-sweetalert2';
import VeeValidate from 'vee-validate';
import moment from "moment";
import VueMomentJS from "vue-momentjs";
 
Vue.use(VueMomentJS, moment);
Vue.use(VeeValidate);
Vue.use(VueSweetalert2);

Vue.config.productionTip = false

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toLowerCase()
  return value.replace(/(^|\s)\S/g, l => l.toUpperCase())
})

Vue.filter('removeNonAlpha', function (value) {
  if (!value) return ''
  return value.replace(/[^\w\s]/gi, '')
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
