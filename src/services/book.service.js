import utilService from './util.service.js'
import storageService from './storage.service.js'
import axios from 'axios';

const BOOKS_KEY = 'HeroloBooksApp';
let booksDB = []

function getBooks() {
    return new Promise((resolve, reject) => {
        let books = storageService.load(BOOKS_KEY);
        
        if (!books) {
          return axios.get("books.json")
          .then(res => {
            books = res.data;
            storageService.store(BOOKS_KEY, books)
            booksDB = books;
            resolve(booksDB);
          });
        }else {
        booksDB = books;
        resolve(booksDB);
        }
    })
}

function addBook(book) {
    book.id = Date.now();
    booksDB.unshift(book)
    storageService.store(BOOKS_KEY, booksDB)
    return Promise.resolve(book)
}

function getById(bookId) {
    const books = storageService.load(BOOKS_KEY)
    const book = books.find(book => book.id == bookId);
    return Promise.resolve(book);
}

function deleteBook(bookId) {
    const books = storageService.load(BOOKS_KEY)
    var bookIdx = books.findIndex(book => book.id == bookId);
    booksDB.splice(bookIdx, 1);
    storageService.store(BOOKS_KEY, booksDB);
    return Promise.resolve();
}

function editBook(book) {
    const books = storageService.load(BOOKS_KEY)
    var bookIdx = books.findIndex(currBook => currBook.id == book.id)
    books.splice(bookIdx, 1, book);
    storageService.store(BOOKS_KEY, books);
}

var dummyDB = 1;

export default {
    addBook,
    getById,
    deleteBook,
    editBook,
    getBooks
}



