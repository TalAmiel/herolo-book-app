import Vue from 'vue'
import Router from 'vue-router'
import BooksApp from './views/BooksApp.vue'
import BookDetails from './views/BookDetails.vue'
import About from './views/About.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: BooksApp
    },
    {
      path: '/books',
      name: 'books-app',
      component: BooksApp
    },
    {
      path: '/books/:bookId',
      name: 'book-details',
      component: BookDetails
    },
    {
      path: '/about',
      name: 'about',
      component: About
    }
  ]
})
