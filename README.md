**Herolo Books - home assignment **

** Live demo: https://determined-rosalind-8767ec.netlify.com/

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## How to install:

npm install

and then npm run serve

should run on port 8080



